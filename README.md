# SlideR :hamburger: : Slide Registration tools

Collection of registration tools for microscopy.

Built upon the Tensor Image Registration Library: https://git.fmrib.ox.ac.uk/ihuszar/tirl

## Dependencies

You will need to download and install miniconda to manage the python environment:
https://docs.conda.io/en/latest/miniconda.html

## Installation
First clone TIRL:
```shell
git clone https://git.fmrib.ox.ac.uk/ihuszar/tirl.git
```

Create a virtual environment for SlideR:
```shell
conda env create -n slider -f tirl/tirlenv.yml
conda activate slider
```

Install TIRL:
```shell
cd tirl
python setup.py install
cd ..
```

Install SlideR:
```shell
git clone https://git.fmrib.ox.ac.uk/seanf/slider.git
pip install -e slider
```

## Usage

### Register 2D-slide to 2D-slide

You can register a 2D-slide to a 2D-slide using the `SLIDE` subcommand of the `slider_app.py` app.  

> Note that the `moving-resolution` and `fixed-resolution` is the resolution of the input images.  The resolution at which the registration is calculated is dictated by the config file (`slider/resources/slide.yaml`)

```shell
>> slider_app.py SLIDE -h

usage: slider_app.py SLIDE [-h] [--out <dir>] [--config <config.json>]
                           <moving> <moving-resolution> <fixed> <fixed-resolution>

Register 2D slide to 2D slide

positional arguments:
  <moving>                Moving slide image
  <moving-resolution>     Moving image resolution (mm)
  <fixed>                 Fixed slide image
  <fixed-resolution>      Fixed image resolution (mm)

optional arguments:
  -h, --help              show this help message and exit
  --out <dir>             Output directory
  --config <config.json>  configuration file
```

For example:

```shell
slider_app.py SLIDE \
    mr247MBP_c24_s20a.jp2 0.0004 \
    mr247NeuN_c01_s19n.jp2 0.0004 \
    --out=mr247_mbp_to_neun.reg
```

### Apply transform to image

Once registered, you can apply the transform to the native image using the `APPLYXFM` subcommand of the `slider_app.py` app.

```shell
>> slider_app.py APPLYXFM -h

usage: slider_app.py APPLYXFM [-h] [--rlevel <rlevel>] [--inverse]
                              <moving> <moving-resolution> <fixed> <fixed-resolution> <reg-moving>
                              <reg-fixed> <resampled-img>

Apply transform to image.

positional arguments:
  <moving>             Moving slide image
  <moving-resolution>  Moving image resolution (mm)
  <fixed>              Fixed slide image
  <fixed-resolution>   Fixed image resolution (mm)
  <reg-moving>         Moving timg from registration directory
  <reg-fixed>          Fixed timg from registration directory (e.g. fixed4_nonlinear.timg)
  <resampled-img>      Name for resampled output image

optional arguments:
  -h, --help           show this help message and exit
  --rlevel <rlevel>    Resolution level for output image (subsample by 2^rlevel)
  --inverse            Invert the transformation chain
  ```

For example:

```shell
slider_app.py APPLYXFM \
    mr247MBP_c24_s20a.jp2 0.0004 \
    mr247NeuN_c01_s19n.jp2 0.0004 \
    mr247_mbp_to_neun.reg/moving.timg \
    mr247_mbp_to_neun.reg/fixed4_nonlinear.timg \
    m4247_mbp_to_neun.jp2 \
    --rlevel=4
```

Or the inverse:

```
slider_app.py APPLYXFM \
    mr247NeuN_c01_s19n.jp2 0.0004 \
    mr247MBP_c24_s20a.jp2 0.0004 \
    mr247_mbp_to_neun.reg/moving.timg \
    mr247_mbp_to_neun.reg/fixed4_nonlinear.timg \
    m4247_neun_to_mbp.jp2 \
    --inverse \
    --rlevel=4
```

### Register chart to slide

You can register a Neurolucida chart file to a 2D-slide using the `CHART` subcommand of the `slider_app.py` app.

> Note that the `slide-resolution` is the resolution of the input images.  The resolution at which the registration is calculated is dictated by the config file (`slider/resources/chart.yaml`)

```shell
>> slider_app.py CHART -h   
usage: slider_app.py CHART [-h] [--outdir <dir>] [--boundary_key <key>] [--justify <left-right>]
                           [--config <config.yaml>]
                           <chart> <slide> <slide-resolution>

Register charting to 2D slide

positional arguments:
  <chart>                 Neurolucida chart file
  <slide>                 Fixed slide
  <slide-resolution>      Slide image resolution (mm)

optional arguments:
  -h, --help              show this help message and exit
  --outdir <dir>          Output directory
  --boundary_key <key>    Name of boundary contour in chart
  --justify <left-right>  Justify chart bounding-box to the left/right of the image bounding box.
                          Useful when only one hemisphere is charted.
  --config <config.yaml>  configuration file
  ```

For example:

```shell
slider_app.py CHART \
    mr250NeuN_c14_s03.ASC \
    mr250NeuN_c14_s03n.jp2 0.0004 \
    --out=mr250_chart_to_neun.reg
```

### Batch processing

SlideR jobs can be batch processed by using the `BATCH` subcommand of the `slider_app.py` app.   

First, you must create a CSV with column names that match the SlideR arguments, and a row for each job to run.  `BATCH` will determine which SlideR subcommand to run (e.g. SLIDE, CHART, or APPLYXFM) from the column names.  For example, the CSV (`jobs.csv`) for 4 jobs  with the `SLIDE` subcommand would be:


| moving | moving_res | fixed | fixed_res | out |
| ---    | ---        | ---   | ---       | --- |
| mr250MBP_c13_s10n.jp2 | 0.0004 | mr250NeuN_c14_s10n.jp2 | 0.0004 | mr250MBP_c13_s10n.reg |
| mr250MBP_c13_s04n.jp2 | 0.0004 | mr250NeuN_c14_s03n.jp2 | 0.0004 | mr250MBP_c13_s04.reg |
| mr247MBP_c24_s20a.jp2 | 0.0004 | mr247NeuN_c01_s19n.jp2 | 0.0004 | mr247MBP_c24_s20a.reg |
| mr248MBP_c10_s11n.jp2 | 0.0004 | mr248NeuN_c11_s14n.jp2 | 0.0004 | mr248MBP_c10_s11n.reg |

This CSV is then passed to `BATCH` to run each of the jobs sequentially:
```shell
slider_app.py BATCH jobs.csv
```

## Coordinate System

SlideR defines:
- origin is the upper left corner of the image
- coordinate units are mm
