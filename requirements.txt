nibabel>=2.*
scipy
matplotlib
numpy
fslpy>=3.0.0
tirl
glymur
tifffile