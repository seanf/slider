#!/usr/bin/env python
#
# Copyright 2021 Sean Fitzgibbon, University of Oxford
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
#
import argparse
import sys

from slider.slide_reg import register_slide_to_slide, apply_slide_xfm
from slider.chart_reg import register_chart_to_slide
from slider.batch import run_batch
from slider.report import report_reg


def add_slide_cli(subparsers):
    """
    Set up slide-to-slide subparser instance.
    """
    parser = subparsers.add_parser(
        "SLIDE",
        description="Register 2D slide to 2D slide",
        formatter_class=lambda prog: argparse.HelpFormatter(
            prog, max_help_position=55, width=100
        ),
    )

    parser.add_argument(
        "moving", metavar="<moving>", help="Moving slide image", type=str
    )
    parser.add_argument(
        "moving_res",
        metavar="<moving-resolution>",
        help="Moving image resolution (mm)",
        type=float,
    )

    parser.add_argument("fixed", metavar="<fixed>", help="Fixed slide image", type=str)
    parser.add_argument(
        "fixed_res",
        metavar="<fixed-resolution>",
        help="Fixed image resolution (mm)",
        type=float,
    )

    parser.add_argument(
        "--out",
        metavar="<dir>",
        help="Output directory",
        default="./slide-to-slide.reg",
        type=str,
        required=False,
    )
    parser.add_argument(
        "--config",
        metavar="<config.yaml>",
        help="configuration file",
        default=None,
        type=str,
        required=False,
    )
    parser.set_defaults(method="slide")


def add_chart_cli(subparsers):
    """
    Set up chart-to-slide subparser instance.
    """
    parser = subparsers.add_parser(
        "CHART",
        description="Register charting to 2D slide",
        formatter_class=lambda prog: argparse.HelpFormatter(
            prog, max_help_position=55, width=100
        ),
    )
    parser.add_argument(
        "chart", metavar="<chart>", help="Neurolucida chart file", type=str
    )
    parser.add_argument("slide", metavar="<slide>", help="Fixed slide", type=str)
    parser.add_argument(
        "slide_res",
        metavar="<slide-resolution>",
        help="Slide image resolution (mm)",
        type=float,
    )
    parser.add_argument(
        "--outdir",
        metavar="<dir>",
        help="Output directory",
        default="./chart-to-slide.reg",
        type=str,
        required=False,
    )
    # parser.add_argument(
    #     "--boundary_key",
    #     metavar="<key>",
    #     help="Name of boundary contour in chart",
    #     default=None,
    #     type=str,
    #     required=False,
    # )
    parser.add_argument(
        "--justify",
        metavar="<left-right>",
        help="Justify chart bounding-box to the left/right of the image bounding box.  Useful when only one hemisphere is charted.",
        default=None,
        choices=['left', 'right', None],
        type=str,
        required=False,
    )
    parser.add_argument(
        "--config",
        metavar="<config.yaml>",
        help="configuration file",
        default=None,
        type=str,
        required=False,
    )
    parser.add_argument(
        "--min_points_in_contour",
        metavar="<n_points>",
        help="Minimum points required in a contour for inclusion.",
        default=3,
        type=int,
        required=False,
    )
    parser.set_defaults(method="chart")


def add_applyxfm_cli(subparsers):
    """
    Set up applyxfm subparser instance.
    """
    parser = subparsers.add_parser(
        "APPLYXFM",
        description="Apply transform to image.",
        formatter_class=lambda prog: argparse.HelpFormatter(
            prog, max_help_position=55, width=100
        ),
    )

    parser.add_argument(
        "moving", metavar="<moving>", help="Moving slide image", type=str
    )
    parser.add_argument(
        "moving_res",
        metavar="<moving-resolution>",
        help="Moving image resolution (mm)",
        type=float,
    )

    parser.add_argument("fixed", metavar="<fixed>", help="Fixed slide image", type=str)
    parser.add_argument(
        "fixed_res",
        metavar="<fixed-resolution>",
        help="Fixed image resolution (mm)",
        type=float,
    )

    parser.add_argument(
        "moving_reg",
        metavar="<reg-moving>",
        help="Moving timg from registration directory",
        type=str,
    )
    parser.add_argument(
        "fixed_reg",
        metavar="<reg-fixed>",
        help="Fixed timg from registration directory (e.g. fixed4_nonlinear.timg)",
        type=str,
    )

    parser.add_argument(
        "out",
        metavar="<resampled-img>",
        help="Name for resampled output image",
        type=str,
    )

    parser.add_argument(
        "--rlevel",
        metavar="<rlevel>",
        help="Resolution level for output image (subsample by 2^rlevel)",
        default=0,
        type=int,
        required=False,
    )

    parser.add_argument(
        "--inverse",
        dest="inverse",
        action="store_true",
        help="Invert the transformation chain",
    )
    parser.set_defaults(method="applyxfm")


def add_batch_cli(subparsers):
    """
    Set up batch subparser instance.
    """
    parser = subparsers.add_parser(
        "BATCH",
        description="Batch processing SlideR jobs",
        formatter_class=lambda prog: argparse.HelpFormatter(
            prog, max_help_position=55, width=100
        ),
    )
    parser.add_argument(
        "csv", metavar="<csv>", help="Spreadsheet of job info", type=str
    )
    parser.set_defaults(method="batch")


def add_report_cli(subparsers):
    """
    Set up report subparser instance.
    """
    parser = subparsers.add_parser(
        "REPORT",
        description="Generate report of SlideR registrations",
        formatter_class=lambda prog: argparse.HelpFormatter(
            prog, max_help_position=55, width=100
        ),
    )
    parser.add_argument(
        "out", metavar="<html>", help="Output filename", default=None, type=str
    )
    parser.add_argument(
        "dirs",
        metavar="<reg-dir>",
        nargs="+",
        help="Registration directories to include in report",
    )
    parser.add_argument(
        "--embed",
        dest="embed",
        action="store_true",
        help="Embed images into report to make it transportable as a single file (report will be much larger).",
    )
    parser.set_defaults(method="report")


if __name__ == "__main__":
    """Main program code."""

    parser = argparse.ArgumentParser()

    subparsers = parser.add_subparsers()
    add_slide_cli(subparsers)
    add_chart_cli(subparsers)
    add_applyxfm_cli(subparsers)
    add_batch_cli(subparsers)
    add_report_cli(subparsers)

    # ---

    if len(sys.argv) == 1:
        parser.parse_args(["-h"])
    else:
        args = vars(parser.parse_args())

    method = args.pop("method")

    if method == "slide":
        register_slide_to_slide(**args)
    elif method == "chart":
        register_chart_to_slide(**args)
    elif method == "applyxfm":
        apply_slide_xfm(**args)
    elif method == "batch":
        run_batch(**args)
    elif method == "report":
        report_reg(**args)
    else:
        raise RuntimeError(f"Unknown method: {method}")
