# Copyright (c) 2016, Ecole Polytechnique Federale de Lausanne, Blue Brain Project
# All rights reserved.
#
# This file is part of NeuroM <https://github.com/BlueBrain/NeuroM>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#     1. Redistributions of source code must retain the above copyright
#        notice, this list of conditions and the following disclaimer.
#     2. Redistributions in binary form must reproduce the above copyright
#        notice, this list of conditions and the following disclaimer in the
#        documentation and/or other materials provided with the distribution.
#     3. Neither the name of the copyright holder nor the names of
#        its contributors may be used to endorse or promote products
#        derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Adapted from: https://github.com/BlueBrain/NeuroM/blob/v1.0.0/neurom/io/neurolucida.py

"""Reader for Neurolucida .ASC files, v3.

reversed engineered from looking at output from Neuroludica
"""

import warnings
from io import open

import numpy as np

# WANTED_SECTIONS = {
#     'Asterix': 0,
# }
# UNWANTED_SECTION_NAMES = [
#     'Color', 'Closed','Stereology','StereologyPropertyExtension','MBFObjectType','FillDensity', 'GUID', 'ImageCoords', 'MBFObjectType',
#     'Marker', 'Name', 'Resolution', 'Set', 'Description',
#     'Cross', 'Dot', 'DoubleCircle', 'FilledCircle', 'FilledDownTriangle',
#     'FilledSquare', 'FilledStar', 'FilledUpTriangle', 'FilledUpTriangle', 'Flower',
#     'Flower2', 'OpenCircle', 'OpenDiamond', 'OpenDownTriangle', 'OpenSquare', 'OpenStar',
#     'OpenUpTriangle', 'Plus', 'ShadedStar', 'Splat', 'TriStar', 'Sections', 'SSM', 'SSM2',
# ]
UNWANTED_SECTION_NAMES = [
    'Stereology','StereologyPropertyExtension','MBFObjectType','FillDensity', 'ImageCoords', 'MBFObjectType',
    'Marker', 'Name', 'Set', 'Description',
    'Cross', 'Dot', 'DoubleCircle', 'FilledCircle', 'FilledDownTriangle',
    'FilledSquare', 'FilledStar', 'FilledUpTriangle', 'FilledUpTriangle', 'Flower',
    'Flower2', 'OpenCircle', 'OpenDiamond', 'OpenDownTriangle', 'OpenSquare', 'OpenStar',
    'OpenUpTriangle', 'Plus', 'ShadedStar', 'Splat', 'TriStar', 'Sections', 'SSM', 'SSM2', 'Site'
]
UNWANTED_SECTIONS = {name: True for name in UNWANTED_SECTION_NAMES}

# --- section parsing ---

def _match_section(section, match):
    '''checks whether the `type` of section is in the `match` dictionary

    Works around the unknown ordering of s-expressions in each section.
    For instance, the `type` is the 3-rd one in for CellBodies
        ("CellBody"
         (Color Yellow)
         (CellBody)
         (Set "cell10")
        )

    Returns:
        value associated with match[section_type], None if no match
    '''
    # TODO: rewrite this so it is more clear, and handles sets & dictionaries for matching
    for i in range(5):
        if i >= len(section):
            return None
        if isinstance(section[i], str) and section[i] in match:
            return match[section[i]]
    return None


def _get_tokens(morph_fd):
    '''split a file-like into tokens: split on whitespace

    Note: this also strips newlines and comments
    '''
    for line in morph_fd:
        line = line.rstrip()   # remove \r\n
        line = line.split(';', 1)[0]  # strip comments
        squash_token = []  # quoted strings get squashed into one token

        if '<(' in line:  # skip spines, which exist on a single line
            assert ')>' in line, 'Missing end of spine'
            continue

        for token in line.replace('(', ' ( ').replace(')', ' ) ').split():
            if squash_token:
                squash_token.append(token)
                if token.endswith('"'):
                    token = ' '.join(squash_token)
                    squash_token = []
                    yield token
            elif token.startswith('"') and not token.endswith('"'):
                squash_token.append(token)
            else:
                yield token


def _parse_section(token_iter):
    '''take a stream of tokens, and create the tree structure that is defined
    by the s-expressions
    '''
    sexp = []
    for token in token_iter:
        if token == '(':
            new_sexp = _parse_section(token_iter)
            if not _match_section(new_sexp, UNWANTED_SECTIONS):
                sexp.append(new_sexp)
        elif token == ')':
            return sexp
        else:
            sexp.append(token)
    return sexp


def _parse_sections(morph_fd):
    '''returns array of all the sections that exist

    The format is nested lists that correspond to the s-expressions
    '''
    sections = []
    token_iter = _get_tokens(morph_fd)
    for token in token_iter:
        if token == '(':  # find top-level sections
            section = _parse_section(token_iter)
            if not _match_section(section, UNWANTED_SECTIONS):
                sections.append(section)
    return sections


def _flatten_subsection(subsection, _type, offset, parent):
    '''Flatten a subsection from its nested version

    Args:
        subsection: Nested subsection as produced by _parse_section, except one level in
        _type: type of section, ie: AXON, etc
        parent: first element has this as it's parent
        offset: position in the final array of the first element

    Returns:
        Generator of values corresponding to [X, Y, Z, R, TYPE, ID, PARENT_ID]
    '''
    for row in subsection:
        # TODO: Figure out what these correspond to in neurolucida
        if row in ('Low', 'Generated', 'High', ):
            continue
        elif isinstance(row[0], str):
            if len(row) in (4, 5, ):
                if len(row) == 5:
                    assert row[4][0] == 'S', \
                        'Only known usage of a fifth member is Sn, found: %s' % row[4][0]
                yield (float(row[0]), float(row[1]), float(row[2]), float(row[3]) / 2.,
                       _type, offset, parent)
                parent = offset
                offset += 1
        elif isinstance(row[0], list):
            split_parent = offset - 1
            start_offset = 0

            slices = []
            start = 0
            for i, value in enumerate(row):
                if value == '|':
                    slices.append(slice(start + start_offset, i))
                    start = i + 1
            slices.append(slice(start + start_offset, len(row)))

            for split_slice in slices:
                for _row in _flatten_subsection(row[split_slice], _type, offset,
                                                split_parent):
                    offset += 1
                    yield _row

# --- data parsing ---

def is_number(s):
    '''Test if string is a number'''
    try:
        float(s)
        return True
    except ValueError:
        return False

def is_point(p):
    '''Test if section looks like a point section'''
    result = True
    if len(p) not in [4, 5]: result = False
    if not all([is_number(n) for n in p[:4]]): result = False
    return result

def parse_point(p):
    '''Parse a point section'''
    return [float(p0) for p0 in p[:-1]] + [p[-1]] 


def parse_contour(c0):
    '''Convert contour section into a dictionary'''
    
    c0_dict = {
        'name': c0[0].replace('"',''),
        'closed': False,
    }

    points = []

    for p in c0[1:]:

        if p[0] == 'Closed':
            c0_dict['closed'] = True
        elif p[0] == 'Color':
            c0_dict['color'] = p[1]
        elif p[0] == 'GUID':
            c0_dict['guid'] = p[1].replace('"','')
        elif p[0] == 'Resolution':
            c0_dict['resolution'] = float(p[1])
        elif is_point(p):
            points.append(parse_point(p)[:4])
        else:
            raise ValueError(f'Unknown property: ({p})')


    c0_dict['points'] = np.array(points, dtype=np.float64)

    return c0_dict

def parse_asterisk(cell):
    '''Convert cell (asterisk) section into a dictionary'''

    cell_dict = {}

    for prop in cell[1:]:
        if prop[0] == 'Color':
            cell_dict['color'] = prop[1]
        elif is_point(prop):
            cell_dict['point'] = np.array(parse_point(prop)[:-1])
        else:
            raise ValueError(f'Unknown property: ({prop})')


    return cell_dict


def to_data(sections):
    # convert sections to dicts
    asterisks    = []
    contours = []
    
    for section in sections:
        if section[0] == 'Asterisk':  # Cell
            asterisks.append(parse_asterisk(section))           
        else:
            contours.append(parse_contour(section))

    if len(asterisks)>0: asterisks = np.asarray(asterisks)

    return contours, asterisks
        

def read(file):
    '''return a list of tuples
    '''

    with open(file, encoding='utf-8', errors='replace') as fd:
        sections = _parse_sections(fd)
    contour, asterisk = to_data(sections)
    return contour, asterisk


