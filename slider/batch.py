#!/usr/bin/env python
# 
# Copyright 2021 Sean Fitzgibbon, University of Oxford
# 
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
# 
#        http://www.apache.org/licenses/LICENSE-2.0
# 
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
# 

import pandas as pd
from slider.slide_reg import register_slide_to_slide, apply_slide_xfm
from slider.chart_reg import register_chart_to_slide

REQUIRED_ARGS = {
    'chart': ['chart', 'slide', 'slide_res'],
    'slide': ['moving', 'moving_res', 'fixed', 'fixed_res'],
    'applyxfm': ['moving', 'moving_res', 'fixed', 'fixed_res', 'moving_reg', 'fixed_reg', 'resampled-img'],
}

def looks_like(df):

    col_names = list(df)

    def has_required_args(ftype):
        return all([arg in col_names for arg in REQUIRED_ARGS[ftype]])

    if has_required_args('chart'):
        print('Looks like a CHART csv')
        return 'CHART'
    elif has_required_args('applyxfm'):
        print('Looks like an APPLYXFM csv')
        return 'APPLYXFM'
    elif has_required_args('slide') and not has_required_args('applyxfm'):
        print('Looks like an SLIDE csv')
        return 'SLIDE'
    else:
        raise RuntimeError('Unknown CSV column names')


def run_batch(csv):
    
    # TODO: Add support for CHART and APPLYXFM csv

    df = pd.read_csv(csv)
    file_type = looks_like(df)

    if file_type == 'SLIDE':

        # csv is SLIDE
        print('Batch slider_app.py SLIDE')
        
        for idx, row in df.iterrows():
            register_slide_to_slide(**row)

    elif file_type == 'CHART':

        # csv is CHART
        print('Batch slider_app.py CHART')
        
        for idx, row in df.iterrows():
            register_chart_to_slide(**row)

    elif file_type == 'APPLYXFM':
        
        # csv is APPLYXFM
        print('Batch slider_app.py APPLYXFM')
        
        for idx, row in df.iterrows():
            apply_slide_xfm(**row)

