#!/usr/bin/env python
#
# Copyright 2021 Sean Fitzgibbon, University of Oxford
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
#
import json
import os
import os.path as op
import warnings
import glymur
import matplotlib.pyplot as plt
import numpy as np
import yaml
from matplotlib import patches
from scipy import ndimage as ndi
from skimage import (
    color,
    exposure,
    filters,
    measure,
    morphology,
    segmentation,
    transform,
)
from typing import Optional
from slider import util
from scipy.spatial import ConvexHull


def register_chart_to_slide(
    chart: str,
    slide: str,
    slide_res: float,
    outdir: str,
    config: Optional[str] = None,
    do_plots: Optional[bool] = None,
    justify: Optional[str] = None,
    field: Optional[str] = None,
    do_convex_hull: bool = False,
    min_points_in_contour: int = 3,
):
    """
    It takes a chart and a slide, and aligns the chart to the slide.

    Args:
      chart (str): The path to the chart file.
      slide (str): The slide to be registered.
      slide_res (float): The resolution of the slide.
      outdir (str): The output directory for the results.
      config (Optional[str]): Path to the configuration file.
      do_plots (Optional[bool]): Whether to plot the results of the alignment.
      justify (Optional[str]): str
      field (Optional[str]): The field of the slide ('light' or 'dark').
      do_convex_hull (bool): bool = False. Defaults to False
      min_points_in_contour (int): The minimum number of points in a contour. If a contour has fewer points than this, it is discarded. Defaults to 3
    """

    chart_name, slide_name = chart, slide

    if config is None:
        config = util.get_resource("chart.yaml")

    with open(config, "r") as f:
        config = yaml.load(f, Loader=yaml.FullLoader)

    rlevel = config["slide"]["resolution_level"]

    # if boundary_key is None:
    #     boundary_key = config["chart"]["boundary_key"]

    if do_plots is None:
        do_plots = config["general"]["plots"]

    #  create output dir
    if not op.exists(outdir):
        os.makedirs(outdir)

    # load chart

    chart = util.Chart.from_neurolucida_ascii(chart)

    # outline = chart.get_contours(closed=True)
    outline = chart.get_contours()

    edge_crds = [x.points[:, :2] * [1, -1] for x in outline]

    # filter out contours with fewer points than min_points_in_contour

    lengths = [crd.shape[0] for crd in edge_crds]
    pt2idx = np.where(np.array(lengths) < min_points_in_contour)[0]
    reqd_idx = np.setdiff1d(np.arange(len(lengths)), pt2idx)
    edge_crds = [edge_crds[idx] for idx in list(reqd_idx)]

    edge_crds_cat = np.concatenate(edge_crds)

    if do_convex_hull:
        hull = ConvexHull(edge_crds_cat)
        edge_crds_cat = np.concatenate(
            [edge_crds_cat[simplex, :] for simplex in hull.simplices], axis=0
        )

    # load slide, convert to grayscale, and invert if light-field

    slide_res = slide_res * (2 ** rlevel)

    jp2 = glymur.Jp2k(slide)
    img_orig = jp2.read(rlevel=rlevel)

    img = color.rgb2gray(img_orig)

    # if exclude_zeros:
    #     nnz_mask = img != 0
    # else:
    #     nnz_mask = None

    # r, c = img.shape

    if field is None:
        field = estimate_field(img)
        print(f"Estimated field is: {field}")

    if field == "light":
        img = 1 - img

    img = enhance(img)

    # initial alignment based on bounding boxes

    init_xfm, img_props, coord_props = initialise_xfm(
        img, slide_res, edge_crds_cat, justify=justify
    )

    # print(init_xfm)
    tr_x, tr_y = init_xfm.translation
    print(
        f"Initial XFM - Rotation: {init_xfm.rotation:0.5f}, Translation: [{tr_x:0.5f} {tr_y:0.5f}], Scale: {init_xfm.scale:0.5f}"
    )

    np.savetxt(f"{outdir}/chart-to-image-init.xfm", init_xfm.params)

    # optimise alignment
    # The `optimise_xfm` function takes a mask and a set of coordinates, and returns the optimal
    # transformation to map the coordinates to the mask.
    mask = img_props["mask"]
    opt_xfm, mdist, iter_props = optimise_xfm(
        mask, slide_res, edge_crds_cat[::3, :], init_xfm
    )
    tr_x, tr_y = opt_xfm.translation
    print(
        f"Optimised XFM - Rotation: {opt_xfm.rotation:0.5f}, Translation: [{tr_x:0.5f} {tr_y:0.5f}], Scale: {opt_xfm.scale:0.5f}"
    )

    np.savetxt(f"{outdir}/chart-to-image.xfm", opt_xfm.params)

    # apply optimised xfm to contours and cells and save

    contour_xfm = [
        (
            contour.name,
            apply_xfm(opt_xfm, contour.points[:, :2] * [1, -1]).tolist(),
            contour.closed,
        )
        for contour in chart.contours
    ]

    contours = [
        {"name": name, "xy": xy, "closed": closed} for name, xy, closed in contour_xfm
    ]
    with open(f"{outdir}/contours.json", "w") as fp:
        json.dump(contours, fp)

    if chart.n_cells > 0:
        cells = np.concatenate(
            [cell.point[:2][np.newaxis, :] for cell in chart.cells]
        ) * [1, -1]
        cells_xfm = apply_xfm(init_xfm, cells)

        with open(f"{outdir}/cells.json", "w") as fp:
            json.dump(cells_xfm.tolist(), fp)

    # do plots

    if do_plots:

        fig, ax = plt.subplots(4, 2, figsize=(20, 30))
        ax = np.ravel(ax)

        # chart bounding box

        plot_contour(
            ax[0],
            edge_crds_cat,
            title="Boundary contour + bounding box",
            linestyle="none",
            marker=".",
            color="b",
        )
        plot_box(ax[0], coord_props["bbox"])
        ax[0].axis("equal")

        # image bounding box

        plot_slide(
            ax[1], img_props["mask"], slide_res, title="Slide mask + bounding box"
        )
        plot_box(ax[1], img_props["bbox"])

        # normals (first iteration)

        plot_slide(
            ax[2], img_props["mask"], slide_res, "Brainmask + normals: first iteration"
        )

        line_x, line_y = iter_props[0]["normals"]
        contour_x, contour_y = iter_props[0]["contour_init"]
        single_crossing_idx = iter_props[0]["single_crossing_idx"]

        plot_normals(
            ax[2],
            line_x[~single_crossing_idx, :],
            line_y[~single_crossing_idx, :],
            contour_x,
            contour_y,
            color="y",
        )
        plot_normals(
            ax[2],
            line_x[single_crossing_idx, :],
            line_y[single_crossing_idx, :],
            contour_x,
            contour_y,
            color="b",
        )

        # normals (last iteration)

        plot_slide(
            ax[3], img_props["mask"], slide_res, "Brainmask + normals: last iteration"
        )

        line_x, line_y = iter_props[-1]["normals"]
        contour_x, contour_y = iter_props[-1]["contour_init"]
        single_crossing_idx = iter_props[-1]["single_crossing_idx"]

        plot_normals(
            ax[3],
            line_x[~single_crossing_idx, :],
            line_y[~single_crossing_idx, :],
            contour_x,
            contour_y,
            color="y",
        )
        plot_normals(
            ax[3],
            line_x[single_crossing_idx, :],
            line_y[single_crossing_idx, :],
            contour_x,
            contour_y,
            color="b",
        )

        # distance plot

        ax[4].plot(mdist)
        ax[4].set_xlabel("iteration")
        ax[4].set_ylabel("mean residual distance")

        ax[5].set_axis_off()

        # boundary plots

        plot_slide(ax[6], img, slide_res, title="Boundary alignment")

        for contour in edge_crds:
            plot_contour(
                ax[6], apply_xfm(init_xfm, contour), color=(1, 0, 0), marker="."
            )
            plot_contour(
                ax[6], apply_xfm(opt_xfm, contour), color=(0, 1, 0), marker="."
            )

        ax[6].legend(["boundary_init", "boundary_optimised"])

        # aligned chart

        plot_slide(ax[7], img, slide_res, title="Aligned Chart")
        cmap = plt.get_cmap("tab10")

        for idx, (name, coords, closed) in enumerate(contour_xfm):
            # if not closed:
            #     continue
            plot_contour(ax[7], coords, name, color=cmap(idx))

        plt.suptitle(f"{chart_name}\n{slide_name}")

        fig.savefig(f"{outdir}/alignment.png", bbox_inches="tight", dpi=300)
        # plt.close(fig)


def plot_normals(ax, line_x, line_y, contour_x, contour_y, color="b"):
    for line_x0, line_y0 in zip(line_x, line_y):
        ax.plot(line_x0, line_y0, ".", color=color)
    ax.plot(contour_x, contour_y, "r.")


def plot_box(ax, bbox, edgecolor="r", facecolor="none", linewidth=1):
    rect = patches.Rectangle(
        (bbox[1], bbox[0]),
        bbox[3] - bbox[1],
        bbox[2] - bbox[0],
        linewidth=linewidth,
        edgecolor=edgecolor,
        facecolor=facecolor,
    )
    ax.add_patch(rect)


def plot_slide(ax, img, slide_res, title=None):

    extent = np.array([0, img.shape[1], img.shape[0], 0]) * slide_res
    ax.imshow(img, extent=extent, cmap="gray")

    ax.set_xlabel("mm")
    ax.set_ylabel("mm")

    if title is not None:
        ax.set_title(title)


def plot_contour(ax, coords, name=None, color="r", title=None, linewidth=1, **kwargs):

    coords = np.array(coords)
    ax.plot(coords[:, 0], coords[:, 1], color=color, linewidth=linewidth, **kwargs)

    if name is not None:
        cog = np.mean(coords, axis=0)
        ax.text(cog[0], cog[1], name, color=color)

    if title is not None:
        ax.set_title(title)

    if not ax.yaxis_inverted():
        ax.invert_yaxis()


def estimate_field(img, mask=None):
    """Estimate field of slide as being light or dark"""

    if mask is None:
        edges = np.zeros(img.shape, dtype=bool)
        edges[:, 0:2], edges[:, -2:], edges[0:2, :], edges[-2:, :] = (
            True,
            True,
            True,
            True,
        )
    else:
        edges = mask ^ morphology.binary_erosion(mask, selem=morphology.disk(10))

    edges_m = np.median(img[edges])

    # calculate mean intensity of the centre of the image
    h, w = img.shape
    h = h // 2
    w = w // 2

    centre = img[h - 100 : h + 100, w - 100 : w + 100]
    centre_m = np.median(centre)

    # estimate field (light or dark)
    if edges_m > centre_m:
        return "light"
    else:
        return "dark"


def enhance(img0, kernel_size=None, lower_percentile=2, upper_percentile=98, sigma=5):
    """Enhance image exposure and contrast"""

    if kernel_size is None:
        h, w = img0.shape
        kernel_size = (h // 8, w // 8)

    img0 = exposure.equalize_adapthist(img0, kernel_size=kernel_size)

    p_lower, p_upper = np.percentile(img0, (lower_percentile, upper_percentile))
    img0 = exposure.rescale_intensity(img0, in_range=(p_lower, p_upper))

    img0 = filters.gaussian(img0, sigma)

    return img0


def segment_foreground(
    img,
    marker_threshold=(0.02, 0.2),
    selem=morphology.disk(30),
):
    """Segment image foreground from background"""

    # calculate elevation map
    elevation_map = filters.sobel(img)

    # extract background and foreground features
    markers = np.zeros_like(img)
    markers[img < marker_threshold[0]] = 1
    markers[img > marker_threshold[1]] = 2

    # segment using watershed algorithm
    labels = segmentation.watershed(elevation_map, markers)
    labels = ndi.binary_fill_holes(labels - 1) + 1

    # find connected components and exclude components < 25% of max component size
    cc = measure.label(labels, background=1)
    p = measure.regionprops(cc, img)

    # ridx = np.where([p0.area > min_component_size for p0 in p])[0]
    # brainmask = np.sum(np.stack([(cc == p[r].label) for r in ridx], axis=2), axis=2)
    a = np.array([p0.area for p0 in p])
    a = a / np.max(a)
    ridx = np.where(a > 0.25)[0]
    brainmask = np.isin(cc, [p[i].label for i in ridx])

    # erode and dilate to clean up edges
    brainmask = morphology.binary_erosion(brainmask, selem=selem)
    brainmask = morphology.binary_dilation(brainmask, selem=selem)

    return brainmask


def optimise_xfm(mask, mask_resolution, contour, xfm, n_iter=100):
    """
    Given a mask, a contour, and an initial transformation, optimise_xfm() iteratively optimises the
    transformation by finding the best fit between the contour and the mask, and then returns the best
    fit transformation.

    Args:
      mask: the binary mask of the structure of interest
      mask_resolution: The resolution of the mask image.
      contour: the contour to be transformed
      xfm: the initial transform
      n_iter: number of iterations to run the optimisation for. Defaults to 100

    Returns:
      The optimise_xfm function returns the optimised xfm, the mean distance between the optimised xfm
      and the contour, and the optimised xfm and the contour at each iteration.
    """

    mdist = np.zeros(n_iter)
    iter_props = [None] * n_iter

    # TODO: move line extent values to config
    max_line_extent = 1.5
    min_line_extent = 0.05

    for idx in range(n_iter):

        xfm, mdist[idx], iter_props[idx] = optimise_xfm_worker(
            mask, mask_resolution, contour, xfm, line_extent=max_line_extent
        )
        max_line_extent = np.maximum(max_line_extent * 0.9, min_line_extent)

    return xfm, mdist, iter_props


def optimise_xfm_worker(image, image_resolution, contour, xfm_init, line_extent=1.5):
    """
    Refine contour by sampling image along normal (to each edge point) and looking for big step change.
    Calculate a new transform (xfm) using refined contour points.
    """

    # calculate normal line to boundary points
    edge_coords_init = apply_xfm(xfm_init, contour)
    normals = normal(edge_coords_init)

    normals[np.isnan(normals)] = 0

    # calculate normal line (to edge_coords)
    edge_init_x, edge_init_y = edge_coords_init.T
    nrml_x, nrml_y = normals.T

    # TODO: move these line extents to the config file
    # line_smpl = np.linspace(-0.03, 0.15, 20)
    line_smpl = np.linspace(-line_extent, line_extent, 1000)

    line_x = edge_init_x[:, np.newaxis] + nrml_x[:, np.newaxis] * line_smpl
    line_y = edge_init_y[:, np.newaxis] + nrml_y[:, np.newaxis] * line_smpl

    # brainmask = segment_foreground(image)
    brainmask = image

    # sample image along normal line

    line_x = np.round(line_x / image_resolution).astype(int)
    line_y = np.round(line_y / image_resolution).astype(int)

    y, x = brainmask.shape
    line_int = brainmask[np.clip(line_y, 0, y - 1), np.clip(line_x, 0, x - 1)]

    # exclude normals that cross the mask edge twice
    single_crossing_idx = np.sum(np.abs(np.diff(line_int, axis=1)), axis=1) == 1

    min_idx = np.argmax(np.abs(np.diff(line_int, axis=-1)), axis=-1)

    refined_edge_coords = np.stack(
        [
            line_x[np.arange(min_idx.size), min_idx] * image_resolution,
            line_y[np.arange(min_idx.size), min_idx] * image_resolution,
        ],
        axis=-1,
    )

    opt_xfm = transform.SimilarityTransform()
    opt_xfm.estimate(
        contour[single_crossing_idx, :], refined_edge_coords[single_crossing_idx, :]
    )

    # calculate error (mean distance)

    opt_edge_coords = apply_xfm(opt_xfm, contour)

    dist = np.sqrt(
        np.sum(
            (
                opt_edge_coords[single_crossing_idx, :]
                - refined_edge_coords[single_crossing_idx, :]
            )
            ** 2,
            axis=1,
        )
    )
    mdist = np.mean(dist)

    iter_props = {
        "single_crossing_idx": single_crossing_idx,
        "normals": (line_x * image_resolution, line_y * image_resolution),
        "contour_init": (edge_init_x, edge_init_y),
    }

    return opt_xfm, mdist, iter_props


def image_props(mask, mask_resolution):
    """
    Given a mask, return a dictionary of properties about the mask

    Args:
      mask: The binary mask.
      mask_resolution: The resolution of the mask.

    Returns:
      A dictionary with the following keys:
        - bbox: The bounding box of the object in the original image.
        - bbox_centroid: The centroid of the bounding box.
        - shape: The shape of the bounding box.
        - aspect_ratio
    """

    p = measure.regionprops(mask.astype(int))

    bbox = np.array(p[0].bbox) * mask_resolution

    centroid = (
        (bbox[0] + bbox[2]) / 2,
        (bbox[1] + bbox[3]) / 2,
    )

    return {
        "bbox": bbox,
        "bbox_centroid": centroid,
        "shape": (bbox[2] - bbox[0], bbox[3] - bbox[1]),
        "aspect_ratio": (bbox[3] - bbox[1]) / (bbox[2] - bbox[0]),
        "mask": mask,
    }


def point_props(pnts):

    x = pnts[:, 0]
    y = pnts[:, 1]

    bbox = (np.min(y), np.min(x), np.max(y), np.max(x))

    centroid = (
        (bbox[0] + bbox[2]) / 2,
        (bbox[1] + bbox[3]) / 2,
    )

    return {
        "bbox": bbox,
        "bbox_centroid": centroid,
        "shape": (bbox[2] - bbox[0], bbox[3] - bbox[1]),
        "aspect_ratio": (bbox[3] - bbox[1]) / (bbox[2] - bbox[0]),
    }


def justify_bbox(bbox, aspect_ratio, justify):
    """Force bbox to aspect_ratio and justify left or right."""

    print(f"justify chart bbox to {justify} in image bbox")

    w, h = bbox[3] - bbox[1], bbox[2] - bbox[0]

    scale_factor = aspect_ratio / (w / h)

    new_w = w * scale_factor

    new_bbox = bbox.copy()

    if justify == "right":
        new_bbox[1] += w - new_w
    elif justify == "left":
        new_bbox[3] -= w - new_w
    else:
        raise ValueError(
            f'Unknown value for justify ("{justify}"). Can only be "left" or "right".'
        )

    return new_bbox


def initialise_xfm(image, image_resolution, contour, tol=0.05, justify=None):
    """
    Calculate the transform (xfm) based on bounding box corners

    Args:
      image: the image to be transformed
      image_resolution: The resolution of the image.
      contour: the contour to be transformed
      tol: tolerance for aspect ratio mismatch between slide and contour
      justify: str

    Returns:
      the transform (xfm), the image properties (image_p) and the contour properties (contour_p).
    """

    brainmask = segment_foreground(image)

    image_p = image_props(brainmask, image_resolution)
    contour_p = point_props(contour)

    # If the aspect-ratio of the image and contour bounding boxes are not equal it typically means that the image is bilateral whilst the contour is lateralised.
    # Thust the contour bbox should left or right justified within the image bbox.

    def aspect_is_equal(x, y):
        return np.abs(x["aspect_ratio"] - y["aspect_ratio"]) < tol

    if (not aspect_is_equal(image_p, contour_p)) & (justify is None):

        # The justification is not set by the user, will attempt left and right and see which has best fit.

        warnings.warn(
            "Slide and chart aspect ratios appear to be different, and direction of justification is not set.  Will attempt to estimate direction."
        )

        bbox_idx = np.array([[1, 2], [1, 0], [3, 2], [3, 0]])
        src = np.array(contour_p["bbox"])[bbox_idx]

        goodness_fit = np.zeros(2)

        directions = ("left", "right")

        hull = ConvexHull(contour)
        contour_hull = np.concatenate(
            [contour[simplex, :] for simplex in hull.simplices], axis=0
        )

        for dir_idx, dir in enumerate(directions):
            bbox = justify_bbox(image_p["bbox"], contour_p["aspect_ratio"], dir)
            dest = np.array(bbox)[bbox_idx]

            xfm = transform.SimilarityTransform()
            xfm.estimate(src, dest)

            # calc goodness of fit (number of single-edge-crossing normals)
            # fit is determined by the number of contour normals that cross the image mask boundary only once

            single_crossing_idx = optimise_xfm_worker(
                brainmask, image_resolution, contour_hull, xfm, line_extent=1
            )[2]["single_crossing_idx"]
            goodness_fit[dir_idx] = np.sum(single_crossing_idx)

        # maximise goodness-of-fit
        justify = directions[np.argmax(goodness_fit)]

        print(
            f"Estimated that the contour should be {justify} justified to the image: {goodness_fit}."
        )

    if justify is not None:

        # force image bbox aspect ratio to be the same as contour bbox, and justify left or right.

        bbox = justify_bbox(image_p["bbox"], contour_p["aspect_ratio"], justify)

        # exclude voxels from brainmask that are outside the bounding box

        bbox = np.round(np.array(bbox) / image_resolution).astype(int)

        cropped_brainmask = brainmask.copy()
        cropped_brainmask[:, : bbox[1]] = 0
        cropped_brainmask[:, bbox[3] :] = 0

        image_p = image_props(cropped_brainmask, image_resolution)

        # recalculate justified bounding box on cropped brainmask
        # force image bbox aspect ratio to be the same as contour bbox, and justify left or right.

        bbox = justify_bbox(image_p["bbox"], contour_p["aspect_ratio"], justify)

        centroid = (
            (bbox[0] + bbox[2]) / 2,
            (bbox[1] + bbox[3]) / 2,
        )

        image_p = {
            "bbox": bbox,
            "bbox_centroid": centroid,
            "shape": (bbox[2] - bbox[0], bbox[3] - bbox[1]),
            "aspect_ratio": (bbox[3] - bbox[1]) / (bbox[2] - bbox[0]),
            "mask": brainmask,
        }

    if not aspect_is_equal(image_p, contour_p):
        raise RuntimeError(
            "Slide and chart aspect ratios appear to be different.  Consider correcting aspect-ratio with left/right justification."
        )

    idx = np.array([[1, 2], [1, 0], [3, 2], [3, 0]])

    src = np.array(contour_p["bbox"])[idx]
    dest = np.array(image_p["bbox"])[idx]

    xfm = transform.SimilarityTransform()
    xfm.estimate(src, dest)

    return xfm, image_p, contour_p


def apply_xfm(xfm, pnts):
    """
    Given a transformation matrix and a set of points, apply the transformation to the points and return
    the result

    Args:
      xfm: the transformation matrix
      pnts: the points to transform

    Returns:
      The transformed points.
    """
    return (
        xfm.params @ np.concatenate((pnts, np.ones((pnts.shape[0], 1))), axis=-1).T
    ).T[:, :2]


def normal(pnts):
    """
    Given a set of points, find the normal vector to the surface defined by those points

    Args:
      pnts: the points to be triangulated

    Returns:
      The normal vector of the plane defined by the two points.
    """

    d_pnts = np.roll(pnts, 1, axis=0) - np.roll(pnts, -1, axis=0)
    normal = np.stack([d_pnts[:, 1], -d_pnts[:, 0]], axis=-1)

    normal = normal / np.linalg.norm(normal, axis=-1, keepdims=True)

    return normal
