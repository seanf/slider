#!/usr/bin/env python
# 
# Copyright 2021 Sean Fitzgibbon, University of Oxford
# 
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
# 
#        http://www.apache.org/licenses/LICENSE-2.0
# 
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
# 

import os.path as op
from dataclasses import dataclass
from typing import Optional, List
import numpy as np
from slider.external import neurolucida
import glymur
import collections
import six

def is_nonstring_iterable(arg):
    return (isinstance(arg, collections.Iterable) and not isinstance(arg, six.string_types))

def get_slider_dir() -> str:
    rpath = op.realpath(op.dirname(op.abspath(__file__)))
    return rpath


def get_resource_path() -> str:
    rpath = op.realpath(op.join(op.dirname(op.abspath(__file__)), "resources"))
    return rpath

def get_resource(name) -> str:
    r = op.join(get_resource_path(), name)
    # asrt.assert_file_exists(r)
    return r


@dataclass
class ChartContour:
    '''Container for chart contour'''
    name: str
    closed: bool
    color: str
    points: np.array
    resolution: Optional[float] = None
    guid: Optional[str] = None

    def __repr__(self):
        r, c = self.points.shape
        return f"ChartContour(name='{self.name}', guid={self.guid}, closed={self.closed}, color='{self.color}', resolution={self.resolution}, points=[{r}x{c} np.array])"


@dataclass
class ChartCell:
    '''Container for chart cell'''
    color: str
    point: np.array


@dataclass
class Chart:
    '''Container for chart'''

    contours: List[ChartContour] = None
    cells: List[ChartCell] = None

    @property
    def n_contours(self):
        return len(self.contours) if self.contours is not None else 0

    @property
    def n_cells(self):
        return len(self.cells) if self.cells is not None else 0

    def get_contours(self, name=None, closed=None):
        cnt = self.contours
        if name is not None: cnt = [c for c in cnt if c.name==name]
        if closed is not None: cnt = [c for c in cnt if c.closed==closed]
        return cnt

    def get_contour_names(self):
        return [contour.name for contour in self.contours] if self.n_contours > 0 else None

    @classmethod
    def from_neurolucida_ascii(cls, fname):
        contours, cells = neurolucida.read(fname)
        return cls(
            [ChartContour(**cnt) for cnt in contours],
            [ChartCell(**cell) for cell in cells] 
        )

    def __repr__(self):
        return f"Chart(contours=[{self.n_contours}x ChartContour], cells=[{self.n_cells}x ChartCell])"


@dataclass
class Slide:

    data: np.array
    resolution: float
    mask: Optional[np.array] = None


    @property
    def shape(self):
        return self.data.shape

    @classmethod
    def from_jp2(cls, fname, resolution):
        im = glymur.Jp2k(fname)
        return cls(im, resolution)
