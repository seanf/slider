#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2020 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar
# Date: 7 June 2020
#
#  Adapted by Sean Fitz 2021


# FMRIB Software Library, Release 6.0.4 (c) 2020, The University of Oxford
# (the "Software")
#
# The Software remains the property of the University of Oxford ("the
# University").
#
# The Software is distributed "AS IS" under this Licence solely for
# non-commercial use in the hope that it will be useful, but in order
# that the University as a charitable foundation protects its assets for
# the benefit of its educational and research purposes, the University
# makes clear that no condition is made or to be implied, nor is any
# warranty given or to be implied, as to the accuracy of the Software,
# or that it will be suitable for any particular purpose or for use
# under any specific conditions. Furthermore, the University disclaims
# all responsibility for the use which is made of the Software. It
# further disclaims any liability for the outcomes arising from using
# the Software.
#
# The Licensee agrees to indemnify the University and hold the
# University harmless from and against any and all claims, damages and
# liabilities asserted by third parties (including claims for
# negligence) which arise directly or indirectly from the use of the
# Software or the sale of any products based on the Software.
#
# No part of the Software may be reproduced, modified, transmitted or
# transferred in any form or by any means, electronic or mechanical,
# without the express permission of the University. The permission of
# the University is not required if the said reproduction, modification,
# transmission or transference is done without financial return, the
# conditions of this Licence are imposed upon the receiver of the
# product, and all original and amended source code is included in any
# transmitted product. You may be held legally responsible for any
# copyright infringement that is caused or encouraged by your failure to
# abide by these terms and conditions.
#
# You are not permitted under this Licence to use this Software
# commercially. Use for which any financial return is received shall be
# defined as commercial use, and includes (1) integration of all or part
# of the source code or the Software into a product for sale or license
# by or on behalf of Licensee to third parties or (2) use of the
# Software or any derivative of it for research with the final aim of
# developing software products for sale or license to a third party or
# (3) use of the Software or any derivative of it for research with the
# final aim of developing non-software products for sale or license to a
# third party, or (4) use of the Software to provide any service to an
# external organisation for which payment is received. If you are
# interested in using the Software commercially, please contact Oxford
# University Innovation ("OUI"), the technology transfer company of the
# University, to negotiate a licence. Contact details are:
# Innovation@innovation.ox.ac.uk quoting reference BS/9564.

"""
Register 2D-slide to 2D-slide

After the input images are centralised, the registration employs sequential
rigid, affine and non-linear alignment. Depending on the chosen direction of
the registration (default: histology->block), the following TIRL chain is
optimised:

+-------------+
| Fixed image |
+-------------+
    offset:
        [centralisation]-->[resampling]
    chain:
        -->(2D anisotropic scale)-->(2D rigid rotation)-->(2D translation)
        -->(2D full affine)
        -->(2D full-resolution displacement field)

+--------------+
| Moving image |
+--------------+
    offset:
        [centralisation]


"""

__tirlscript__ = True

# DEPENDENCIES

import argparse
import json, yaml
import skimage
from numpy.lib.shape_base import _kron_dispatcher
import logging
import os
import sys
import warnings
from math import radians, degrees

import numpy as np
import tirl
import tirl.scripts.mnd.image
import tirl.scripts.mnd.inout
import tirl.settings as ts
from attrdict import AttrMap
from skimage.measure import label, regionprops
from tirl.beta import beta_function
from tirl.chain import Chain
from tirl.constants import *
from tirl.costs.mind import CostMIND
# from tirl.costs.mi import CostMI
from tirl.optimisation.gnoptdiff import GNOptimiserDiffusion
from tirl.optimisation.optgroup import OptimisationGroup
from tirl.optimisation.optnl import OptNL
from tirl.regularisers.diffusion import DiffusionRegulariser
from tirl.tfield import TField
from tirl.timage import TImage
from tirl.transformations.linear.affine import TxAffine
from tirl.transformations.linear.rotation import TxRotation2D
from tirl.transformations.linear.scale import TxScale, TxIsoScale
from tirl.transformations.linear.translation import TxTranslation
from tirl.transformations.nonlinear.displacement import TxDisplacementField

import matplotlib

matplotlib.use("Agg")
print(matplotlib.get_backend())

import matplotlib.pyplot as plt
import os.path as op

from slider import util

# Rotation search: number of best initialisations to test
N_BEST = 3
# File extension of the snapshot images
SNAPSHOT_EXT = "png"
# NumPy print formatting
np.set_printoptions(precision=4)


def _load_jpg2k(file, resolution, resolution_level, dtype=None):
    # load jp2
    import glymur

    jp2 = glymur.Jp2k(file)
    img = jp2.read(rlevel=resolution_level)

    # adjust resolution by resolution_level
    resolution = resolution * (2 ** resolution_level)

    # create timg
    timg = TImage.fromarray(img, tensor_axes=(2,), dtype=dtype)
    timg.resolution = resolution

    return timg


def _load_image(p):

    if p.file.lower().endswith(".jp2"):  # if jpg2k

        # load jp2
        timg = _load_jpg2k(p.file, p.resolution, p.resolution_level, p.dtype)

        # add mask
        if 'mask' in p.keys():
            tirl.scripts.mnd.image.set_mask(timg, scope=globals(), **p.mask)

        # Export
        tirl.scripts.mnd.inout.export(timg, p.export, default=None)

        # Snapshot
        tirl.scripts.mnd.inout.snapshot(timg, p.snapshot, default=None)

    else:  # else other image types
        timg = tirl.scripts.mnd.inout.load_image(scope=globals(), **p)

    return timg


def run(cnf=None, **options):
    """
    Runs TIRL histology-to-fixed registration (stage 1).

    :param cnf:
        Configuration file. If no file is specified (default), the default
        parameters will be used that are embedded in the source code file
        (see Definitions section above). Instead of a file, a dictionary with
        suitable content may also be specified.
    :type cnf: Union[str, dict, None]
    :param options:
        Overriding configuration parameters.
    :type options: Any

    """
    # Load script configuration
    if cnf is not None:
        if isinstance(cnf, dict):
            cnf = dict(cnf)
        elif isinstance(cnf, str):
            with open(cnf, "r") as configfile:
                cnf = dict(json.load(configfile))
        else:
            raise TypeError(
                f"Unrecognised configuration format: {cnf.__.class__.__name__}"
            )
    cnf.update(options)
    p, logger = tirl.scripts.mnd.general.initialise_script(**cnf)
    p.logger = logger.name  # avoid globals

    # Load and configure input images
    if p.moving.export is True:
        ext = ts.EXTENSIONS["TImage"]
        p.moving.export = os.path.join(p.general.outputdir, f"moving.{ext}")

    moving = _load_image(p.moving)

    moving.snapshot(
        os.path.join(p.general.outputdir, f"moving0_input.{SNAPSHOT_EXT}"),
        overwrite=True,
    )

    if p.fixed.export is True:
        ext = ts.EXTENSIONS["TImage"]
        p.fixed.export = os.path.join(p.general.outputdir, f"fixed.{ext}")

    fixed = _load_image(p.fixed)

    fixed.snapshot(
        os.path.join(p.general.outputdir, f"fixed0_input.{SNAPSHOT_EXT}"),
        overwrite=True,
    )

    # Having loaded both images, perform actions on the histology image prior
    # to registration, unless it was loaded from a TImage.
    # Actions are user-defined functions within the TIRL namespace. Actions
    # can be chain-loaded to perform preparatory analysis steps on the images
    # before registration begins.
    isalternative = p.moving.file.lower().endswith(
        (ts.EXTENSIONS["TImage"], ts.EXTENSIONS["TIRLObject"])
    )
    if not isalternative:
        moving = tirl.scripts.mnd.image.perform_image_operations(
            moving, *p.preprocessing.moving, scope=globals(), other=fixed, cnf=p
        )

        # Initialise registration frame
        moving.centralise(weighted=False)
        moving.snapshot(
            os.path.join(p.general.outputdir, f"moving0_centralised.{SNAPSHOT_EXT}"),
            overwrite=True,
        )

        if moving.mask is not None:
            TImage.fromarray(moving.mask).snapshot(
                os.path.join(p.general.outputdir, f"moving0_mask.{SNAPSHOT_EXT}"),
                overwrite=True,
            )

    # Perform actions on the fixed image prior to registration, unless it was
    # loaded from a TImage file.
    isalternative = p.fixed.file.lower().endswith(
        (ts.EXTENSIONS["TImage"], ts.EXTENSIONS["TIRLObject"])
    )
    if not isalternative:

        fixed = tirl.scripts.mnd.image.perform_image_operations(
            fixed, *p.preprocessing.fixed, scope=globals(), other=moving, cnf=p
        )

        fixed.centralise(weighted=False)
        fixed.snapshot(
            os.path.join(p.general.outputdir, f"fixed0_centralised.{SNAPSHOT_EXT}"),
            overwrite=True,
        )

        if fixed.mask is not None:
            TImage.fromarray(fixed.mask).snapshot(
                os.path.join(p.general.outputdir, f"fixed0_mask.{SNAPSHOT_EXT}"),
                overwrite=True,
            )

    # Run the registration routine
    try:
        register(fixed, moving, p)
    except Exception as exc:
        logger.error(exc.args)
        logger.fatal(f"The registration terminated with an exception.")
        raise exc
    else:
        logger.fatal("The registration was completed successfully.")


def labkmeans(histo, n_clusters=2, **kwargs):
    """
    Segments the foreground (tissue) in a histological slide and sets it as the
    TImage mask.

    This method was tested on slides that were stained with DAB + haematoxylin,
    and scanned with a bright white background. The method was also effectively
    removing the shadow effect close to the slide edge.

    """
    from skimage.color import rgb2lab
    from sklearn.cluster import KMeans
    from skimage.exposure import rescale_intensity

    orig_order = histo.order
    histo.order = VOXEL_MAJOR
    imdata = np.asarray(
        rescale_intensity(histo.data[..., :3], out_range=np.uint8), dtype=np.uint8
    )
    lab_a = rgb2lab(imdata)[..., 1]
    X = lab_a.reshape(-1, 1)
    km = KMeans(n_clusters=n_clusters, random_state=0).fit(X)
    kc = km.cluster_centers_
    # mask = km.labels_.reshape(histo.vshape)
    # if kc[0] > kc[1]:  # make sure that the lower intensity is labeled 0
    #     mask = 1 - mask

    mask = km.labels_.reshape(histo.vshape) != np.argmin(kc)

    histo.order = orig_order
    return mask.astype(int)


def initialise_transformations(fixed, p):
    """
    Create transformation chain that will be optimised.

    """
    q = p.regparams

    # Scale
    lb = np.asarray(q.init.scale.lb)
    ub = np.asarray(q.init.scale.ub)
    if p.general.isotropic:
        bounds = (float(lb), float(ub))
        tx_scale = TxIsoScale(
            float(q.init.scale.x0), dim=2, bounds=bounds, name="scale"
        )
    else:
        tx_scale = TxScale(*q.init.scale.x0, bounds=(lb, ub), name="scale")

    # Rotation
    if str(q.init.rotation.mode).lower() == "deg":
        lb = radians(float(q.init.rotation.lb))
        ub = radians(float(q.init.rotation.ub))
    else:
        lb = float(q.init.rotation.lb)
        ub = float(q.init.rotation.ub)
    tx_rotation = TxRotation2D(
        float(q.init.rotation.x0),
        mode=q.init.rotation.mode,
        bounds=(lb, ub),
        name="rotation",
    )

    # Translation
    lb = np.asarray(q.init.translation.lb)
    ub = np.asarray(q.init.translation.ub)
    tx_trans = TxTranslation(
        *q.init.translation.x0, bounds=(lb, ub), name="translation"
    )

    # Affine
    x0 = np.asarray(q.init.affine.x0).reshape((2, 3))
    lb = np.asarray(q.init.affine.lb)
    ub = np.asarray(q.init.affine.ub)
    tx_affine = TxAffine(x0, bounds=(lb, ub), name="affine")

    # Append linear transformations to the domain of the fixed image
    linear_chain = Chain(tx_rotation, tx_scale, tx_trans, tx_affine)
    domain = fixed.domain[:]
    domain.chain.extend(linear_chain)

    # Nonlinear
    x0 = float(q.init.nonlinear.x0) * np.ones((2, *fixed.vshape))
    if q.init.nonlinear.lb is None:
        lb = None
    else:
        lb = float(q.init.nonlinear.lb) * np.ones_like(x0)
    if q.init.nonlinear.ub is None:
        ub = None
    else:
        ub = float(q.init.nonlinear.ub) * np.ones_like(x0)
    field = TField.fromarray(
        x0, tensor_axes=(0,), copy=False, domain=domain[:], order=TENSOR_MAJOR
    )
    tx_nonlinear = TxDisplacementField(
        field, bounds=(lb, ub), name="nonlinear", mode=NL_REL
    )

    # Return the full transformation chain
    return Chain(*linear_chain, tx_nonlinear)


# TODO: This will fail if one of the registration steps was skipped.  FIX it
def _plot_alignment(d):

    fixed = plt.imread(f"{d}/fixed.png")
    moving = ["rotation", "rigid", "affine", "nonlinear"]

    N = len(moving)

    fig, ax = plt.subplots(1, N, figsize=(N * 5, 5))

    for idx, (m0, ax0) in enumerate(zip(moving, ax)):

        img = plt.imread(f"{d}/moving{idx+1}_{m0}.png")

        ax0.imshow(fixed, cmap="Greys_r")
        ax0.imshow(img, alpha=0.5, cmap="Reds_r")

        ax0.set_title(m0.title())

        ax0.axes.xaxis.set_visible(False)
        ax0.axes.yaxis.set_visible(False)

    plt.tight_layout()
    fig.savefig(f"{d}/alignment.png", dpi=150)


def register(fixed, moving, cnf):
    """
    Runs the four registration stages: rotation search, rigid, affine, and
    non-linear. The function has no return value. The transformation chain is
    attached to the Domain of the fixed TImage object and is optimised in situ.

    :param fixed:
        fixed image (to which the chain is attached)
    :type fixed: TImage
    :param moving:
        moving image (that defines the coordinate space that the mapping
        is into)
    :type moving: TImage
    :param cnf: all configuration options
    :type cnf: dict or AttrMap

    """
    p = AttrMap(cnf)
    q = p.regparams
    logger = logging.getLogger(p.logger)

    # Create transformation chain (does not change the fixed image)
    # rotation -> scale -> translation -> affine -> nonlinear
    logger.info("Initialising transformation chain...")
    chain = initialise_transformations(fixed, p)
    logger.info("Transformation chain has been initialised.")

    # Generate output: initial alignment
    if p.fixed.export:
        fixed.save(os.path.join(p.general.outputdir, "fixed.timg"), overwrite=True)
    if p.fixed.snapshot:
        fixed.snapshot(
            os.path.join(p.general.outputdir, f"fixed.{SNAPSHOT_EXT}"), overwrite=True
        )
    if p.moving.export:
        moving.save(os.path.join(p.general.outputdir, "moving.timg"), overwrite=True)
    if p.moving.snapshot:
        moving.snapshot(
            os.path.join(p.general.outputdir, f"moving.{SNAPSHOT_EXT}"), overwrite=True
        )

    # Set the first part of the chain
    fixed.domain.chain.extend(chain[:-2])

    # Rotation search
    if "rotation" in p.general.stages:
        logger.info("Starting rotation search...")
        rotation_search2d(fixed, moving, p)
        logger.info("Completed rotation search.")
        # Generate output
        if q.rotsearch.export:
            fixed.save(
                os.path.join(p.general.outputdir, "fixed1_rotation.timg"), overwrite=True
            )
        if q.rotsearch.snapshot:
            moving.evaluate(fixed.domain).snapshot(
                os.path.join(p.general.outputdir, f"moving1_rotation.{SNAPSHOT_EXT}"),
                overwrite=True,
            )
    else:
        logger.info("Rotation search was skipped.")

    # Rigid registration
    if "rigid" in p.general.stages:
        logger.info("Starting rigid registration...")
        rigid2d(fixed, moving, p)
        logger.info("Completed rigid registration.")
        # Generate output
        if q.rigid.export:
            fixed.save(
                os.path.join(p.general.outputdir, "fixed2_rigid.timg"), overwrite=True
            )
        if q.rigid.snapshot:
            moving.evaluate(fixed.domain).snapshot(
                os.path.join(p.general.outputdir, f"moving2_rigid.{SNAPSHOT_EXT}"),
                overwrite=True,
            )
    else:
        logger.info("Rigid registration was skipped.")

    # Affine registration
    fixed.domain.chain.append(chain[-2])
    if "affine" in p.general.stages:
        logger.info("Starting affine registration...")
        affine2d(fixed, moving, p)
        logger.info("Completed affine registration.")
        # Generate output
        if q.affine.export:
            fixed.save(
                os.path.join(p.general.outputdir, "fixed3_affine.timg"), overwrite=True
            )
        if q.affine.snapshot:
            moving.evaluate(fixed.domain).snapshot(
                os.path.join(p.general.outputdir, f"moving3_affine.{SNAPSHOT_EXT}"),
                overwrite=True,
            )
    else:
        logger.info("Affine registration was skipped.")

    # Non-linear registration
    tx_nonlinear = chain[-1]
    tx_nonlinear.domain.chain = fixed.domain.chain[:]
    fixed.domain.chain.append(tx_nonlinear)
    if "nonlinear" in p.general.stages:
        logger.info("Starting non-linear registration...")
        diffreg2d(fixed, moving, p)
        logger.info("Completed non-linear registration.")
        # Generate output
        if q.nonlinear.export:
            fixed.save(
                os.path.join(p.general.outputdir, "fixed4_nonlinear.timg"), overwrite=True
            )
        if q.nonlinear.snapshot:
            moving.evaluate(fixed.domain).snapshot(
                os.path.join(p.general.outputdir, f"moving4_nonlinear.{SNAPSHOT_EXT}"),
                overwrite=True,
            )
    else:
        logger.info("Non-linear registration was skipped.")

    _plot_alignment(p.general.outputdir)


def rotation_search2d(fixed, moving, cnf):
    """
    Finds the best relative orientation of the images.

    :param fixed: Fixed image.
    :type fixed: TImage
    :param moving: Moving image.
    :type moving: TImage
    :param cnf: all configuration options
    :type cnf: dict or AttrMap

    """
    p = AttrMap(cnf)
    q = p.regparams.rotsearch
    logger = logging.getLogger(p.logger)

    # Part 1: coarse search for N best orientations

    # Coarse search at a predefined scale
    fixed.resample(float(q.scale), copy=False)
    moving.resample(float(q.scale), copy=False)

    step = radians(q.coarse)
    tx_rotation = fixed.domain.chain["rotation"]
    lb, ub = tx_rotation.parameters.get_bounds()[0]
    rotations = np.arange(lb, ub, step)
    costvals = []
    for i, angle in enumerate(rotations):
        tx_rotation.parameters.parameters[0] = angle
        tx_rotation.parameters.set_lower_bounds(angle - step / 2)
        tx_rotation.parameters.set_upper_bounds(angle + step / 2)
        cost = CostMIND(moving, fixed, normalise=True, kernel=MK_FULL)()
        logger.debug(f"{degrees(angle)} deg: {cost}")
        costvals.append([cost, angle])
    else:
        costvals = np.asarray(costvals)

    # Test the best N initial rotations
    n_best = max(min(N_BEST, len(rotations)), 1)
    best_angles = costvals[np.argsort(costvals[:, 0]), 1][:n_best].ravel()
    logger.info(
        f"The {n_best} best initialisation angles: " f"{np.rad2deg(best_angles)} deg"
    )

    # Part 2: fine-tune the rotation parameter by simultaneous additional
    # optimisation of translation and scaling, starting from the best three
    # orientations.
    tx_scale = fixed.domain.chain["scale"]
    tx_translation = fixed.domain.chain["translation"]
    og = OptimisationGroup(tx_rotation, tx_scale, tx_translation)
    scale_orig = tx_scale.parameters.parameters.copy()
    translation_orig = tx_translation.parameters.parameters.copy()
    costvals = []
    for angle in best_angles:
        tx_rotation.parameters.parameters[0] = angle
        tx_rotation.parameters.set_lower_bounds(angle - step / 2)
        tx_rotation.parameters.set_upper_bounds(angle + step / 2)
        tx_rotation.parameters.unlock()
        tx_scale.parameters.parameters[:] = scale_orig.copy()
        tx_translation.parameters.parameters[:] = translation_orig.copy()
        # Rescale to the same predefined resolution as above
        fixed.resample(float(q.scale), copy=False)
        moving.resample(float(q.scale), copy=False)
        # Set cost function
        cost = CostMIND(moving, fixed, maskmode="and", normalise=True)
        # Start optimisation
        logger.info(
            f"Co-optimising scale and translation at " f"{degrees(angle)} deg..."
        )

        OptNL(
            og,
            cost,
            method="LN_BOBYQA",
            visualise=q.visualise,
            xtol_abs=q.xtol_abs,
            xtol_rel=q.xtol_rel,
            step=q.opt_step,
            logger=logger,
            normalised=True,
        )()
        costvals.append((cost(), og.parameters[:]))

    # Find best initialisation based on cost
    best_params = min(costvals, key=lambda r: r[0])[1]
    logger.info(f"Best parameters after rotation search: {best_params}")
    og.set(best_params)
    tx_rotation.parameters.set_lower_bounds(best_params[0] - step / 2)
    tx_rotation.parameters.set_upper_bounds(best_params[0] + step / 2)
    tx_rotation.parameters.unlock()

    # Return to full resolution after the rotation search
    fixed.resample(1, copy=False)
    moving.resample(1, copy=False)


def rigid2d(fixed, moving, cnf):
    """
    Optimises rigid-body parameters and scaling.

    """
    p = AttrMap(cnf)
    q = p.regparams.rigid
    logger = logging.getLogger(p.logger)

    # Scaling-smoothing iteration
    for i, (sc, sm) in enumerate(zip(q.scaling, q.smoothing)):
        logger.debug(f"Scale: {sc}, smoothing: {sm} px...")
        # Prepare images for the current iteration
        fixed.resample(1.0 / sc, copy=False)
        moving.resample(1.0 / sc, copy=False)
        fixed_smooth = fixed.smooth(sm, copy=True)
        moving_smooth = moving.smooth(sm, copy=True)
        # Prepare co-optimised transformations
        tx_rotation = fixed_smooth.domain.chain["rotation"]
        tx_scale = fixed_smooth.domain.chain["scale"]
        tx_translation = fixed_smooth.domain.chain["translation"]
        og = OptimisationGroup(tx_rotation, tx_scale, tx_translation)
        lb, ub = og.get_bounds().T
        lb = lb - np.finfo(lb.dtype).eps
        ub = ub + np.finfo(lb.dtype).eps
        og.set_bounds(lb, ub)

        # binarise mask
        if moving_smooth.mask is not None:
            moving_smooth.mask = (moving_smooth.mask > 0.5).astype(int)

        if fixed_smooth.mask is not None:
            fixed_smooth.mask = (fixed_smooth.mask > 0.5).astype(int)

        # Set cost function
        cost = CostMIND(moving_smooth, fixed_smooth, normalise=True, maskmode="and")

        # Start optimisation
        OptNL(
            og,
            cost,
            method="LN_BOBYQA",
            visualise=q.visualise,
            xtol_abs=q.xtol_abs,
            xtol_rel=q.xtol_rel,
            step=q.opt_step,
            logger=logger,
            normalised=True,
        )()

        # Transfer optimised transformations to the non-smoothed images
        fixed.domain = fixed_smooth.domain
        moving.domain = moving_smooth.domain
    else:
        # Restore full resolution of the images
        fixed.resample(1, copy=False)
        moving.resample(1, copy=False)


def affine2d(fixed, moving, cnf):
    """
    Optimises a 6-DOF affine transformation after rigid alignment has been
    fine-tuned.

    """
    p = AttrMap(cnf)
    q = p.regparams.affine
    logger = logging.getLogger(p.logger)

    # Scaling-smoothing iterations
    for i, (sc, sm) in enumerate(zip(q.scaling, q.smoothing)):
        logger.debug(f"Scale: {sc}, smoothing: {sm} px...")
        # Prepare images for the current iteration
        fixed.resample(1.0 / sc, copy=False)
        moving.resample(1.0 / sc, copy=False)
        fixed_smooth = fixed.smooth(sm, copy=True)
        moving_smooth = moving.smooth(sm, copy=True)
        # Prepare transformation to optimise
        tx_affine = fixed_smooth.domain.chain["affine"]

        # binarise mask
        if moving_smooth.mask is not None:
            moving_smooth.mask = (moving_smooth.mask > 0.5).astype(int)

        if fixed_smooth.mask is not None:
            fixed_smooth.mask = (fixed_smooth.mask > 0.5).astype(int)

        # Set cost function
        cost = CostMIND(moving_smooth, fixed_smooth, normalise=True, maskmode="and")
        # Start optimisation
        OptNL(
            tx_affine,
            cost,
            method="LN_BOBYQA",
            xtol_rel=q.xtol_rel,
            xtol_abs=q.xtol_abs,
            visualise=q.visualise,
            step=q.opt_step,
            logger=logger,
            normalised=True,
        )()
        # Transfer optimised transformations to the non-smoothed images
        fixed.domain = fixed_smooth.domain
        moving.domain = moving_smooth.domain
    else:
        # Restore full resolution of the images
        fixed.resample(1, copy=False)
        moving.resample(1, copy=False)


def diffreg2d(fixed, moving, cnf):
    """
    Performs a non-linear registration. The transformation is parameterised as
    a dense displacement field. The cost is MIND, and diffusion regularisation
    is used to create smoothness in the deformation field.

    """
    p = AttrMap(cnf)
    q = p.regparams.nonlinear
    logger = logging.getLogger(p.logger)

    # Scaling-smoothing iteration
    for i, (sc, sm) in enumerate(zip(q.scaling, q.smoothing)):
        logger.debug(f"Scale: {sc}, smoothing: {sm} px...")
        # Prepare images for the current iteration
        fixed.resample(1 / sc, copy=False)
        moving.resample(1 / sc, copy=False)
        fixed_smooth = fixed.smooth(sm, copy=True)
        moving_smooth = moving.smooth(sm, copy=True)

        # binarise mask (if req'd)
        if moving_smooth.mask is not None:
            moving_smooth.mask = (moving_smooth.mask > 0.5).astype(int)

        if fixed_smooth.mask is not None:
            fixed_smooth.mask = (fixed_smooth.mask > 0.5).astype(int)

        # Prepare transformation to optimise
        tx_nonlinear = fixed_smooth.domain.chain[-1]

        # Set cost and regulariser

        if q.kernel == "MK_FULL":
            kernel = MK_FULL
        elif q.kernel == "MK_STAR":
            kernel = MK_STAR
        else:
            raise RuntimeError(f"Unknown Cost Kernel: {q.kernel}")

        cost = CostMIND(
            moving_smooth,
            fixed_smooth,
            sigma=float(q.sigma),
            truncate=float(q.truncate),
            # kernel=MK_FULL,
            # kernel=MK_STAR,
            kernel=kernel,
            maskmode=q.maskmode,
        )

        # cost = CostMI(
        #     moving_smooth,
        #     fixed_smooth,
        #     # sigma=float(q.sigma),
        #     # truncate=float(q.truncate),
        #     kernel=kernel,
        #     maskmode=q.maskmode,
        # )

        # np.save(
        #     os.path.join(p.general.outputdir, f"cost_sc{sc}.npy"),
        #     cost.costmap().data,
        # )

        # from skimage import io

        # for idx in range(cost.costmap().data.shape[0]):
        #     io.imsave(
        #         os.path.join(p.general.outputdir, f"cost_sc{sc}_{idx}.png"),
        #         np.squeeze(cost.costmap().data[idx,...])
        #     )

        # import nibabel as nb

        # print(cost.costmap().data.shape)

        # if cost.costmap().data.ndim == 2:
        #     cd0 = cost.costmap().data[..., np.newaxis].transpose([2, 1, 0])
        # else:
        #     cd0 = cost.costmap().data.transpose([3, 2, 1, 0])
        #     cd0 = np.flip(cd0, axis=1)

        # nb.Nifti1Image(cd0.astype(float), affine=np.eye(4)).to_filename(os.path.join(p.general.outputdir, f"cost_sc{sc}.nii.gz"))

        regularisation = DiffusionRegulariser(tx_nonlinear, weight=float(q.regweight))
        # Optimise the non-linear transformation
        GNOptimiserDiffusion(
            tx_nonlinear,
            cost,
            regularisation,
            maxiter=int(q.maxiter[i]),
            xtol_rel=q.xtol_rel,
            xtol_abs=q.xtol_abs,
            visualise=q.visualise,
            logger=logger,
        )()
        # Transfer optimised transformations to the non-smoothed images
        fixed.domain = fixed_smooth.domain
        moving.domain = moving_smooth.domain

        # fixed_smooth.snapshot(
        #     os.path.join(p.general.outputdir, f"fixed_sc{sc}.{SNAPSHOT_EXT}"), overwrite=True,
        # )

        # moving_smooth.evaluate(fixed_smooth.domain).snapshot(
        #     os.path.join(p.general.outputdir, f"moving_sc{sc}.{SNAPSHOT_EXT}"), overwrite=True,
        # )

    else:
        # Restore the original resolution of the images
        fixed.resample(1, copy=False)
        moving.resample(1, copy=False)


# AUXILIARY FUNCTIONS


# These functions may be used within other functions of the script. Their usage
# outside this script (via importing from this module) is discouraged. They are
# included here to prevent too frequent code repetitions.


def threshold_mask(img):

    from skimage import color, exposure, filters, measure, morphology

    d0 = color.rgb2hsv(img.data.astype(np.uint8))[..., -1]
    d0 = exposure.equalize_hist(d0)

    thr = filters.threshold_li(d0)
    d0 = d0 < thr

    cc = measure.label(d0, background=0)
    p = measure.regionprops(cc, d0)

    ridx = np.argmax([p0.area for p0 in p])
    p = p[ridx]

    d0 = cc==p.label
    d0 = morphology.area_closing(d0)

    d0 = morphology.binary_dilation(d0, morphology.disk(60, dtype=bool))
    d0 = morphology.binary_erosion(d0, morphology.disk(50, dtype=bool))

    return d0.astype(int)


def gmm_mask(img, gmm_comps=3, dilation_radius=2):

    from skimage.measure import regionprops, label
    from skimage.color import rgb2gray
    from skimage import filters, transform, exposure, morphology
    from scipy import ndimage as ndi
    from sklearn import mixture

    img0 = img.data.astype(np.uint8)
    img0 = rgb2gray(img0)

    full_sz = img0.shape

    # downsample

    # img = transform.rescale(img, (1.0/2, 1.0/2), order=0, anti_aliasing=True)

    # rescale intensity
    p_min, p_max = np.percentile(img0, (1, 99))
    img0 = exposure.rescale_intensity(img0, in_range=(p_min, p_max))

    # use guassian mixture model to segment tissues

    # clf = mixture.BayesianGaussianMixture(
    #     n_components=gmm_comps,
    #     covariance_type='full',
    # )

    clf = mixture.GaussianMixture(
        n_components=gmm_comps,
    )

    clf.fit(img0.reshape((-1, 1)))

    pred = clf.predict(img0.reshape((-1, 1)))
    pred = pred.reshape(img0.shape)

    # note: this assumes a bright background (argmin should be used for dark BG)
    target = [np.mean(img0[pred == idx]) for idx in range(gmm_comps)]
    mask = pred != np.argmax(target)

    # calculate connected components
    cc = label(mask, background=0)

    # get region properties for each cc and select the largest cc (in terms of area)
    p = regionprops(cc, img0)

    ridx = np.argmax([p0.area for p0 in p])
    p = p[ridx]

    # create mask
    mask = cc == p.label

    # fill holes
    mask = ndi.binary_fill_holes(mask)

    # dilate
    mask = morphology.binary_opening(mask, morphology.disk(10, dtype=bool))
    # mask = morphology.binary_dilation(mask, morphology.disk(5, dtype=bool))

    # upsample
    # mask = transform.resize(mask, full_sz, order=0)

    return mask.astype(int)


def rgb2hsv(x):
    assert x.shape[-1] == 3, "The input must be RGB."
    _x = x / 255
    _x = _x.reshape(-1, 3)
    c_min = np.min(_x, axis=-1)
    c_max = np.max(_x, axis=-1)
    delta = c_max - c_min
    r, g, b = _x.reshape(-1, 3).T
    r = r.ravel()
    g = g.ravel()
    b = b.ravel()
    i2 = c_max == r
    i3 = c_max == g
    i4 = c_max == b

    # Calculate Hue
    h = np.zeros(r.size)
    h[i2] = 60 * ((g[i2] - b[i2]) / delta[i2] % 6)
    h[i3] = 60 * ((b[i3] - r[i3]) / delta[i3] + 2)
    h[i4] = 60 * ((r[i4] - g[i4]) / delta[i4] + 4)

    # Calculate Saturation
    s = np.where(np.isclose(c_max, 0), 0, delta / c_max)

    # Calculate Value
    v = c_max

    x_hsv = np.stack((h.ravel(), s.ravel(), v.ravel()), axis=-1)
    return x_hsv


def match_fixed_resolution(img, **kwargs):
    """
    Resamples histology image to match the resolution of the fixed image.

    """
    p = AttrMap(kwargs.get("cnf"))
    logger = logging.getLogger(p.logger)
    other = kwargs.get("other")

    res = other.resolution
    factors = np.divide(img.resolution, res)
    logger.info(f"Resampling 2D image by factors {tuple(factors)}...")
    img = img.resample(*factors, copy=True)
    img.reset_resmgr()
    img.storage = MEM
    img.domain.storage = MEM
    logger.info(f"Shape of the 2D image after resampling: {img.shape}.")

    # Save low-resolution image
    # fp, fn = os.path.split(p.moving.file)
    # fn, ext = os.path.splitext(fn)
    # filename = os.path.join(p.general.outputdir, f"{fn}_lowres.{SNAPSHOT_EXT}")
    # img.snapshot(filename, overwrite=True)
    # logger.info(f"Saved low-resolution snapshot of the 2D image to: {filename}")

    return img


# def resample40(img, **kwargs):
#     p = AttrMap(kwargs.get("cnf"))
#     logger = logging.getLogger(p.logger)

#     logger.info(f"Shape of the 2D image before resampling: {img.shape}.")
#     logger.info(f"Resolution of the 2D image before resampling: {img.resolution}.")
#     factors = np.divide(img.resolution, 0.04)

#     img = img.resample(*factors, copy=True)
#     img.reset_resmgr()
#     img.storage = MEM
#     img.domain.storage = MEM
#     logger.info(f"Shape of the 2D image after resampling: {img.shape}.")
#     logger.info(f"Resolution of the 2D image after resampling: {img.resolution}.")

#     # Save low-resolution image
#     fp, fn = os.path.split(p.histology.file)
#     fn, ext = os.path.splitext(fn)
#     filename = os.path.join(p.general.outputdir, f"{fn}_lowres.{SNAPSHOT_EXT}")
#     img.snapshot(filename, overwrite=True)
#     logger.info(f"Saved low-resolution snapshot of the 2D image to: {filename}")

#     return img


def moving_preprocessing(histo, **kwargs):
    """
    Preprocess the histology image.

    """
    # Convert to grayscale using the Y channel of the YIQ colour space.
    return tirl.scripts.mnd.image.rgb2yiq(histo.tensors[:3]).tensors[0]

    # from skimage import exposure
    # from skimage import color

    # p = AttrMap(kwargs.get("cnf"))
    # logger = logging.getLogger(p.logger)

    # logger.info('Pre-process moving image')

    # pp = exposure.equalize_adapthist(np.prod(color.rgb2hsv(histo.data)[...,1:], axis=-1))

    # pp = TImage.fromarray(pp[..., np.newaxis], tensor_axes=(2,), dtype='f4')
    # pp.resolution = histo.resolution
    # pp.mask = histo.mask

    # return pp



def preprocessing_resample(img, **kwargs):
    p = AttrMap(kwargs.get("cnf"))
    logger = logging.getLogger(p.logger)
    img = img.resample(1 / 10, copy=True)
    img.reset_resmgr()
    img.storage = MEM
    img.domain.storage = MEM
    logger.info(f"Shape of the 2D image after resampling: {img.shape}.")

    # Save low-resolution image
    fp, fn = os.path.split(p.histology.file)
    fn, ext = os.path.splitext(fn)
    filename = os.path.join(p.general.outputdir, f"{fn}_resampled.{SNAPSHOT_EXT}")
    img.snapshot(filename, overwrite=True)
    logger.info(f"Saved low-resolution snapshot of the 2D image to: {filename}")

    return img


def fixed_preprocessing(block, **kwargs):
    """
    Preprocess the block photograph.

    """
    # Convert to grayscale using the Y channel of the YIQ colour space.
    return tirl.scripts.mnd.image.rgb2yiq(block.tensors[:3]).tensors[0]

    # from skimage import exposure
    # from skimage import color

    # p = AttrMap(kwargs.get("cnf"))
    # logger = logging.getLogger(p.logger)

    # logger.info('Pre-process fixed image')

    # pp = exposure.equalize_adapthist(np.prod(color.rgb2hsv(block.data)[...,1:], axis=-1))

    # pp = TImage.fromarray(pp[..., np.newaxis], tensor_axes=(2,), dtype='f4')
    # pp.resolution = block.resolution
    # pp.mask = block.mask

    # return pp


def hsv_sat(timg, **kwargs):
    from skimage.color import rgb2hsv

    hsv = rgb2hsv(timg.data)
    return hsv[..., 1]


def hsv_invsat(timg, **kwargs):
    from skimage.color import rgb2hsv

    hsv = rgb2hsv(timg.data)
    return 1 - hsv[..., 1]


def dilated_object_mask(timg, **kwargs):
    from skimage.color import rgb2hsv

    hsv = rgb2hsv(timg.data)
    objmask = hsv[..., 2] > 0.1
    from skimage.morphology import binary_dilation

    objmask = binary_dilation(objmask.astype(np.uint8), selem=np.ones((20, 20)))
    return objmask.astype(np.float32)


def mask_and(moving, fixed):

    from skimage import transform

    moving_mask = moving.evaluate(fixed.domain).mask
    fixed_mask = fixed.mask

    moving_mask = transform.resize(
        moving_mask.astype(bool), fixed_mask.shape, order=0, preserve_range=True
    )

    mask = np.logical_not((moving_mask > 0) != (fixed_mask > 0))

    TImage(mask).preview()

    fixed.mask = mask.astype(int)
    moving.mask = np.ones(moving.vshape, dtype=int)


@beta_function
def mask_roi_defects(moving, fixed, p):
    # Highlight non-matching areas between affine-registered images
    tmp = moving.evaluate(fixed.domain)
    tmp = np.where(tmp.data > 0.05, 1, 0)
    binary = np.where(fixed.data > 1, 1, 0)
    totalarea = np.count_nonzero(binary)
    # print(totalarea)
    area = p.area * totalarea if p.area < 1 else p.area
    # print(area)
    binary[tmp == 1] = 0
    del tmp

    # Discard small regions, that should actually drive non-linear registration

    # Area filter
    labels = label(binary, background=0, connectivity=binary.ndim)
    regions = regionprops(labels)
    # print(sorted([region["area"] for region in regions])[::-1])
    large_region_labels = [
        region["label"] for region in regions if region["area"] > area
    ]
    binary[...] = 0
    for value in large_region_labels:
        binary[labels == value] = 1

    labels = label(binary, background=0, connectivity=binary.ndim)
    n_defects = np.max(labels)
    TImage(labels).preview()

    # Warn the user about ROI defects
    if n_defects:
        warnings.warn("Number of ROI defects found: {}.".format(int(n_defects)))

    # Add the identified ROI defects to the block mask
    binary = 1 - binary
    bmask = fixed.mask.data
    # bmask[bmask == 0.1] = 0
    fixed.mask = bmask * binary


def pad(timg, **kwargs):
    """Zero-pads the image by 1/5 of the image shape on both ends along each
    spatial dimension."""
    padding = []
    for dim in timg.vshape:
        padding.append((dim // 6, dim // 6))
    padding = tuple(padding)
    offset = TxTranslation([-pad[0] for pad in padding], name="padding")
    lpad = ((0, 0),) * timg.tdim
    if timg.order == TENSOR_MAJOR:
        padding = lpad + padding
    else:
        padding = padding + lpad
    imdata = np.pad(timg.data, padding, mode="maximum")
    offset = offset + timg.domain.offset
    chain = timg.domain.chain
    padded = TImage.fromarray(imdata, tensor_axes=timg.taxes)
    padded.domain.offset = offset
    padded.domain.chain = chain
    return padded


def register_slide_to_slide(moving, moving_res, fixed, fixed_res, out, config=None):
    """
    Register 2D-slide to 2D-slide using TIRL.

    """

    if config is None:
        config = util.get_resource("slide.yaml")

    if isinstance(config, str) and os.path.isfile(config):
        with open(config, "r") as fp:
            config = yaml.load(fp)
    # else:
    #     raise FileNotFoundError(
    #         f"The provided configuration file " f"does not exist: {config}"
    #     )

    config["moving"]["file"] = moving
    config["moving"]["resolution"] = moving_res
    config["fixed"]["file"] = fixed
    config["fixed"]["resolution"] = fixed_res
    config["general"]["outputdir"] = out
    config["general"]["logfile"] = f"{out}/logfile.log"
    config["general"]["paramlogfile"] = f"{out}/paramlog.log"

    # Run registration script
    run(config)


def apply_slide_xfm(
    moving,
    moving_res,
    fixed,
    fixed_res,
    moving_reg,
    fixed_reg,
    out,
    rlevel=0,
    inverse=False):

    moving_reg = tirl.load(moving_reg)
    fixed_reg = tirl.load(fixed_reg)

    # get transformation chain from registration

    chain = fixed_reg.domain.chain
    chain += moving_reg.domain.chain.inverse()

    if inverse:
        chain = chain.inverse()

    # update input/output resolution in transformation chain

    new_res_moving = moving_res * (2 ** rlevel)
    new_res_fixed = fixed_res * (2 ** rlevel)

    chain[0].parameters[:] = [new_res_fixed, new_res_fixed]
    chain[-1].parameters[:] = [1 / new_res_moving, 1 / new_res_moving]

    # load native resolution images

    moving_native = _load_image(AttrMap(
        {
            'file': moving,
            'resolution': moving_res, 
            'resolution_level': rlevel,
            'dtype': 'uint8',
            'export': False,
            'snapshot': 'moving_native.png',
        }
    ))

    fixed_native = _load_image(AttrMap(
        {
            'file': fixed,
            'resolution': fixed_res, 
            'resolution_level': rlevel,
            'dtype': 'uint8',
            'export': False,
            'snapshot': 'fixed_native.png',
        }
    ))

    # moving_native = _load_jpg2k(moving, moving_res, rlevel)
    # fixed_native = _load_jpg2k(fixed, fixed_res, rlevel)

    # fixed pixel coordinates in native res

    v = fixed_native.domain.get_voxel_coordinates()

    # Map through transformation chain

    fixed_coords_in_moving_native = chain.map(v)

    # Interpolate

    moving_value = moving_native.interpolator(fixed_coords_in_moving_native)
    moving_value_reshaped = moving_value.reshape(
        (fixed_native.shape[0], fixed_native.shape[1], 3)
    )

    # TODO: additional ouput types (beyond jp2) should be supported

    ext = op.splitext(out)[-1]

    if ext == '.jp2':
        import glymur
        glymur.Jp2k(out, data=np.round(moving_value_reshaped).astype(moving_native.dtype))
    else:
        import skimage.io
        skimage.io.imsave(out, np.round(moving_value_reshaped).astype(moving_native.dtype))

    # import imageio
    # imageio.imsave('test_moving.png', np.round(moving_value_reshaped).astype(moving_native.dtype))
    # imageio.imsave('test_fixed.png', fixed_native.data)
