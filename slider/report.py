#!/usr/bin/env python
#
# Copyright 2021 Sean Fitzgibbon, University of Oxford
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
#
import jinja2
import os.path as op
import os
import base64


def report_reg(dirs, out, embed=False):

    images = [f"{d}/alignment.png" for d in dirs]
    images = [d for d in images if op.exists(d)]

    images = [(op.dirname(f), to_base64(f) if embed else f) for f in images]

    env = jinja2.Environment(
        loader=jinja2.PackageLoader("slider", "resources"),
        autoescape=jinja2.select_autoescape(["html", "xml"]),
        extensions=["jinja2.ext.do"],
    )

    template = env.get_template("registration_report_template.html")
    html = template.render(
        images=images,
    )

    if not out.endswith('.html'):
        out = out + '.html'

    with open(out, "w") as outfile:
        outfile.write(html)


def to_base64(fname):

    with open(fname, "rb") as tmp:
        tmp.seek(0)
        s = base64.b64encode(tmp.read()).decode("utf-8")

    return "data:image/{};base64,".format(op.splitext(fname)[-1]) + s
