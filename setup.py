import setuptools

with open('requirements.txt', 'rt') as f:
    install_requires = [l.strip() for l in f.readlines()]

setuptools.setup(
    name="slider",
    version='0.0.1',
    author="Sean Fitzgibbon",
    author_email="sean.fitzgibbon@ndcn.ox.ac.uk",
    description="Slide Registration tool",
    url="https://git.fmrib.ox.ac.uk/seanf/slider",
    install_requires=install_requires,
    scripts=['slider_app.py'],
    packages=setuptools.find_packages(),
    include_package_data=True,
    python_requires='>=3.7',
)